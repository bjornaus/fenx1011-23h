{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0e9986f4",
   "metadata": {},
   "source": [
    "# Varmepumpeprosess\n",
    "\n",
    "Denne notatboka omhandler analyse av varmepumpeprosesser ved hjelp av [CoolProp](http://www.coolprop.org/). Før du går gjennom notatboka, kan det lønne seg å se gjennom [notatboka som introduserer CoolProp](./introduction-coolprop.ipynb), forelesningsnotatene om varmepumpeprosesser og øvingsoppgavene om Rankine-syklus (Øving 11).\n",
    "\n",
    "I denne notatboka er nødvendig kode i utgangspunktet ferdigutfylt, med mulighet for å supplere med egne utvidelser. Oppgavene går ut på å sammenligne Python-analysen med beregningene gjort i Øving 11 og diskutere betydningen av ulike parametere for prosessytelsen. Oppgavene er gitt i egne bokser, som illustrert nedenfor, etterfulgt av egne celler hvor oppgavene kan besvares med tekst.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>Oppgaveformat</b>\n",
    "<br>Oppgavene er formatert i slike bokser\n",
    "</div>\n",
    "\n",
    "Notatboka består av to deler. Første deler omhandler analyse av en enkel, ideell varmepumpeprosess, mens andre del omhandler analyse av en enkel varmepumpeprosess med tap (i kompressor). Analysene omfatter Ts- og ph-diagram, samt beregning av ytelsesfaktor."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c4d10de",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Enkel, ideell varmepumpeprosess\n",
    "\n",
    "En enkel varmepumpeprosess (dampkompresjonssyklus) er illustrert i figuren nedenfor.\n",
    "\n",
    "![Enkel varmepumpeprosess](../Figures/refrigeration-simple.png)\n",
    "\n",
    "Den enkle, ideelle varmepumpe-syklusen består av fire internt reversible delprosesser:\n",
    "- 1&rarr;2: Kompressor: Isentropisk kompresjon\n",
    "- 2&rarr;3: Kondensator: Isobar nedkjøling\n",
    "- 3&rarr;4: Strupeventil: Isentalpisk ekspansjon\n",
    "- 4&rarr;1: Fordamper: Isobar oppvarming\n",
    "\n",
    "Vi antar i analysene her at tilstanden ut fra fordamperen er mettet gass og at tilstanden ut fra kondensatoren er mettet væske."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d133f2f",
   "metadata": {},
   "outputs": [],
   "source": [
    "#CoolProp is not part of the NTNU JupyterHub setup and must be installed\n",
    "#every time the hub is activated to run the code\n",
    "!pip install coolprop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "824f7d4f",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Importing necessary Python libraries\n",
    "import math #Mathematical operations\n",
    "import numpy as np #Mathematical operations\n",
    "import matplotlib.pyplot as plt #Plotting\n",
    "import CoolProp as CP #Thermodynamic data\n",
    "from CoolProp.CoolProp import PropsSI #Thermodynamic data, properties\n",
    "from CoolProp.CoolProp import PhaseSI #Thermodynamic data, phase\n",
    "import ipywidgets as widgets #Interactive functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "78b7b498",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Global plot settings\n",
    "plt.rcParams['figure.figsize']=(16,12)\n",
    "plt.rcParams['font.size']='14'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "3692abda",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Relevant refrigerant fluids\n",
    "fluids = {\n",
    "    \"R134a\": \"R134a\",\n",
    "    \"Ammoniakk\": \"Ammonia\",\n",
    "    \"Propan\": \"n-Propane\"\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0280167f",
   "metadata": {},
   "source": [
    "### Temperatur-entropi-diagram\n",
    "\n",
    "Nedenfor er prosessforløpet i en enkel, ideell varmepumpesyklus plottet i et temperatur-entropi-diagram."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "2aaa083f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tsdiagram(fluid):\n",
    "    # Plotting Ts diagram for given fluid with phase envelope\n",
    "    # fluid: Fluid name (as string)\n",
    "    \n",
    "    Tmin = 240\n",
    "    Tmax = PropsSI('Tcrit',fluid) #Critical temperature\n",
    "    temperature = np.linspace(Tmin,Tmax,1000)\n",
    "    bubblepoint = PropsSI('S','T',temperature,'Q',0,fluid) #Bubblepoint curve (x = 0)\n",
    "    dewpoint = PropsSI('S','T',temperature,'Q',1,fluid) #Dewpoint curve (x = 1)\n",
    "    \n",
    "    plt.plot(bubblepoint, temperature, color = 'C0', linewidth = 1)\n",
    "    plt.plot(dewpoint, temperature, color = 'C0', linewidth = 1)\n",
    "    plt.title('Temperatur-entropi-diagram')\n",
    "    plt.xlabel('Spesifikk entropi (J/kgK)')\n",
    "    plt.ylabel('Temperatur (K)')\n",
    "    plt.show"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "dabea27d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tsdiagram_heatpump_simple_ideal(fluid,TL,TH):\n",
    "    # Plotting ideal simple heat pump cycle in Ts diagram\n",
    "    # fluid: refrigerant (as string)\n",
    "    # TL: Evaporation temperature (°C)\n",
    "    # TH: Condensation temperature (°C) \n",
    "    # Assuming saturated vapor at evaporator outlet\n",
    "    # Assuming saturated liquid at condenser outlet\n",
    "    \n",
    "    T1 = TL+273.15\n",
    "    s1 = PropsSI('S','T',T1,'Q',1,fluid) #Specific entropy state 1\n",
    "    p1 = PropsSI('P','T',T1,'Q',1,fluid) #Pressure state 1\n",
    "    T3 = TH+273.15\n",
    "    p3 = PropsSI('P','T',T3,'Q',0,fluid) #Pressure state 3\n",
    "    h3 = PropsSI('H','T',T3,'Q',0,fluid) #Specific enthalpy state 3\n",
    "    s3 = PropsSI('S','T',T3,'Q',0,fluid) #Specific entropy state 3\n",
    "    p2 = p3 #Pressure state 2\n",
    "    s2 = s1 #Specific entropy state 2\n",
    "    T2 = PropsSI('T','P',p2,'S',s2,fluid) #Temperature state 2\n",
    "    h4 = h3 #Specific enthalpy state 4\n",
    "    T4 = T1 #Temperature state 4\n",
    "    p4 = p1 #Pressure state 4\n",
    "    s4 = PropsSI('S','P',p4,'H',h4,fluid) #Specific entropy state 4\n",
    "    \n",
    "    compressor_entropy = [s1, s2] #Specific entropy 1->2\n",
    "    compressor_temperature = [T1, T2] #Temperature 1->2\n",
    "    condenser_entropy = np.linspace(s2, s3, 1000) #Entropy 2->3\n",
    "    condenser_temperature = PropsSI('T','P',p2,'S',condenser_entropy,fluid) #Temperature 2->3\n",
    "    valve_entropy = np.linspace(s3, s4, 1000) #Specific entropy 3->4\n",
    "    valve_temperature = PropsSI('T','H',h3,'S',valve_entropy,fluid) #Temperature 3->4\n",
    "    evaporator_entropy = [s1, s4] #Specific entropy 4->1\n",
    "    evaporator_temperature = [T1, T4] #Temperature 4->1\n",
    " \n",
    "    tsdiagram(fluid)\n",
    "    plt.plot(compressor_entropy,compressor_temperature,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(condenser_entropy,condenser_temperature,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(valve_entropy,valve_temperature,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(evaporator_entropy,evaporator_temperature,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    \n",
    "    plt.annotate(\"1\", (s1,T1), (s1+40,T1), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"2\", (s2,T2), (s2,T2+40), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"3\", (s3,T3), (s3-80,T3), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"4\", (s4,T4), (s4-80,T4), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "771bcc87",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "5cc3f50b81c245ecb2cc942f34ca4017",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(Dropdown(description='Fluid:', options={'R134a': 'R134a', 'Ammoniakk': 'Ammonia', 'Propa…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "#Interactive plot of ideal simple heat pump cycle in Ts diagram\n",
    "widgets.interact(tsdiagram_heatpump_simple_ideal, \n",
    "    fluid = widgets.Dropdown(options = fluids, description = \"Fluid:\"),\n",
    "    TL = widgets.FloatSlider(value = 0, min = -30, max = 10, description = \"$T_{\\mathrm{lav}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False),\n",
    "    TH = widgets.FloatSlider(value = 30, min = 20, max = 50, description = \"$T_{\\mathrm{høy}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False)\n",
    "        );"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aec24ea0",
   "metadata": {},
   "source": [
    "### Trykk-entalpi-diagram\n",
    "\n",
    "Nedenfor er prosessforløpet i en ideell, enkel varmepumpesyklus plottet i et trykk-entalpi-diagram."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "2e3b324c",
   "metadata": {},
   "outputs": [],
   "source": [
    "def phdiagram(fluid):\n",
    "    # Plotting ph diagram for given fluid with phase envelope\n",
    "    # fluid: Fluid name (as string)\n",
    "    \n",
    "    pmin = 1E4\n",
    "    pmax = PropsSI('Pcrit',fluid) #Critical pressure\n",
    "    pressure = np.linspace(pmin,pmax,100)\n",
    "    bubblepoint = PropsSI('H','P',pressure,'Q',0,fluid) #Bubblepoint curve (x = 0)\n",
    "    dewpoint = PropsSI('H','P',pressure,'Q',1,fluid) #Dewpoint curve (x = 1)\n",
    "    \n",
    "    plt.plot(bubblepoint, pressure, color = 'C0', linewidth = 1)\n",
    "    plt.plot(dewpoint, pressure, color = 'C0', linewidth = 1)\n",
    "    plt.title('Trykk-entalpi-diagram')\n",
    "    plt.xlabel('Spesifikk entalpi (J/kgK)')\n",
    "    plt.ylabel('Trykk (Pa)')\n",
    "    plt.yscale('log')\n",
    "    plt.show"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "8c060cee",
   "metadata": {},
   "outputs": [],
   "source": [
    "def phdiagram_heatpump_simple_ideal(fluid,TL,TH):\n",
    "    # Plotting ideal simple heat pump cycle in ph diagram\n",
    "    # fluid: refrigerant (as string)\n",
    "    # TL: Evaporation temperature (°C)\n",
    "    # TH: Condensation temperature (°C)\n",
    "    # Assuming saturated vapor at evaporator outlet\n",
    "    # Assuming saturated liquid at condenser outlet\n",
    "    \n",
    "    T1 = TL+273.15\n",
    "    s1 = PropsSI('S','T',T1,'Q',1,fluid) #Specific entropy state 1\n",
    "    p1 = PropsSI('P','T',T1,'Q',1,fluid) #Pressure state 1\n",
    "    h1 = PropsSI('H','T',T1,'Q',1,fluid) #Specific enthalpy state 1\n",
    "    T3 = TH+273.15\n",
    "    p3 = PropsSI('P','T',T3,'Q',0,fluid) #Pressure state 3\n",
    "    h3 = PropsSI('H','T',T3,'Q',0,fluid) #Specific enthalpy state 3\n",
    "    s3 = PropsSI('S','T',T3,'Q',0,fluid) #Specific entropy state 3\n",
    "    p2 = p3 #Pressure state 2\n",
    "    s2 = s1 #Specific entropy state 2\n",
    "    T2 = PropsSI('T','P',p2,'S',s2,fluid) #Temperature state 2\n",
    "    h2 = PropsSI('H','P',p2,'S',s2,fluid) #Specific enthalpy state 2\n",
    "    h4 = h3 #Specific enthalpy state 4\n",
    "    T4 = T1 #Temperature state 4\n",
    "    p4 = p1 #Pressure state 4\n",
    "    s4 = PropsSI('S','P',p4,'H',h4,fluid) #Specific entropy state 4\n",
    "    \n",
    "    compressor_enthalpy = np.linspace(h1, h2, 1000) #Specific enthalpy 1->2\n",
    "    compressor_pressure = PropsSI('P','S',s1,'H',compressor_enthalpy,fluid) #Pressure 1->2\n",
    "    condenser_enthalpy = [h2,h3] #Specific enthalpy 2->3\n",
    "    condenser_pressure = [p2,p3] #Pressure 2->3\n",
    "    valve_enthalpy = [h3,h4] #Specific enthalpy 3->4\n",
    "    valve_pressure = [p3,p4] #Pressure 3->4\n",
    "    evaporator_enthalpy = [h1, h4] #Specific enthalpy 4->1\n",
    "    evaporator_pressure = [p1, p4] #Pressure 4->1\n",
    " \n",
    "    phdiagram(fluid)\n",
    "    plt.plot(compressor_enthalpy,compressor_pressure,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(condenser_enthalpy,condenser_pressure,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(valve_enthalpy,valve_pressure,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(evaporator_enthalpy,evaporator_pressure,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    \n",
    "    plt.annotate(\"1\", (h1,p1), (h1+60000,p1), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"2\", (h2,p2), (h2+60000,p2), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"3\", (h3,p3), (h3-60000,p3), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"4\", (h4,p4), (h4-60000,p4), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "e88e0d99",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "86053f608aaf411c8ce7c414c5931526",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(Dropdown(description='Fluid:', options={'R134a': 'R134a', 'Ammoniakk': 'Ammonia', 'Propa…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "#Interactive plot of ideal simple heat pump cycle in ph diagram\n",
    "widgets.interact(phdiagram_heatpump_simple_ideal, \n",
    "    fluid = widgets.Dropdown(options = fluids, description = \"Fluid:\"),\n",
    "    TL = widgets.FloatSlider(value = 0, min = -30, max = 10, description = \"$T_{\\mathrm{lav}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False),\n",
    "    TH = widgets.FloatSlider(value = 30, min = 20, max = 50, description = \"$T_{\\mathrm{høy}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False)\n",
    "        );"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "162b3c37",
   "metadata": {},
   "source": [
    "### Ytelsesfaktor - COP\n",
    "\n",
    "Ytelsesfaktoren er definert som forholdet mellom nyttig varme og netto tilført arbeid, og avhenger av om det varme- eller kjøleytelsen som er av interesse: \n",
    "$$ \\mathrm{COP}_{\\mathrm{HP}} = \\frac{\\dot Q_{\\mathrm{ut}}}{\\dot W_{\\mathrm{netto}}}, \\\\ \n",
    "\\mathrm{COP}_{\\mathrm{R}} = \\frac{\\dot Q_{\\mathrm{inn}}}{\\dot W_{\\mathrm{netto}}}. $$ \n",
    "\n",
    "Fra energibalansen for de ulike delprosessene i den enkle varmepumpeprosessen (gitt stasjonær tilstand, ingen varmetap til omgivelsene og neglisjerbar effekt av kinetisk og potensiell energi), er det gitt at \n",
    "$$ \\dot W_{\\mathrm{netto}} = \\dot m \\cdot (h_2 - h_1), \\\\ \n",
    "\\dot Q_{\\mathrm{inn}} = \\dot m \\cdot (h_1 - h_4), \\\\ \n",
    "\\dot Q_{\\mathrm{ut}} = \\dot m \\cdot (h_2 - h_3). \\\\ $$\n",
    "\n",
    "Ytelsesfaktoren kan derfor uttrykkes som \n",
    "$$ \\mathrm{COP}_{\\mathrm{HP}} = \\frac{h_2-h_3}{h_2-h_1}, \\\\\n",
    " \\mathrm{COP}_{\\mathrm{R}} = \\frac{h_1-h_4}{h_2-h_1}. $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "ff4bbe46",
   "metadata": {},
   "outputs": [],
   "source": [
    "def cop_hp_simple_ideal(fluid,TL,TH):\n",
    "    # Calculating coefficient of performance of ideal simple heat pump cycle\n",
    "    # fluid: refrigerant (as string)\n",
    "    # TL: Evaporation temperature (°C)\n",
    "    # TH: Condensation temperature (°C)\n",
    "    # Assuming saturated vapor at evaporator outlet\n",
    "    # Assuming saturated liquid at condenser outlet\n",
    "    \n",
    "    T1 = TL+273.15\n",
    "    s1 = PropsSI('S','T',T1,'Q',1,fluid) #Specific entropy state 1\n",
    "    p1 = PropsSI('P','T',T1,'Q',1,fluid) #Pressure state 1\n",
    "    h1 = PropsSI('H','T',T1,'Q',1,fluid) #Specific enthalpy state 1\n",
    "    T3 = TH+273.15\n",
    "    p3 = PropsSI('P','T',T3,'Q',0,fluid) #Pressure state 3\n",
    "    h3 = PropsSI('H','T',T3,'Q',0,fluid) #Specific enthalpy state 3\n",
    "    s3 = PropsSI('S','T',T3,'Q',0,fluid) #Specific entropy state 3\n",
    "    p2 = p3 #Pressure state 2\n",
    "    s2 = s1 #Specific entropy state 2\n",
    "    T2 = PropsSI('T','P',p2,'S',s2,fluid) #Temperature state 2\n",
    "    h2 = PropsSI('H','P',p2,'S',s2,fluid) #Specific enthalpy state 2\n",
    "    h4 = h3 #Specific enthalpy state 4\n",
    "    T4 = T1 #Temperature state 4\n",
    "    p4 = p1 #Pressure state 4\n",
    "    s4 = PropsSI('S','P',p4,'H',h4,fluid) #Specific entropy state 4\n",
    "    \n",
    "    cop =(h2-h3)/(h2-h1) #Coefficient of performance\n",
    "    return(cop)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "ee6c5707",
   "metadata": {},
   "outputs": [],
   "source": [
    "def cop_r_simple_ideal(fluid,TL,TH):\n",
    "    # Calculating coefficient of performance of ideal simple heat pump cycle\n",
    "    # fluid: refrigerant (as string)\n",
    "    # TL: Evaporation temperature (°C)\n",
    "    # TH: Condensation temperature (°C)\n",
    "    # Assuming saturated vapor at evaporator outlet\n",
    "    # Assuming saturated liquid at condenser outlet\n",
    "    \n",
    "    T1 = TL+273.15\n",
    "    s1 = PropsSI('S','T',T1,'Q',1,fluid) #Specific entropy state 1\n",
    "    p1 = PropsSI('P','T',T1,'Q',1,fluid) #Pressure state 1\n",
    "    h1 = PropsSI('H','T',T1,'Q',1,fluid) #Specific enthalpy state 1\n",
    "    T3 = TH+273.15\n",
    "    p3 = PropsSI('P','T',T3,'Q',0,fluid) #Pressure state 3\n",
    "    h3 = PropsSI('H','T',T3,'Q',0,fluid) #Specific enthalpy state 3\n",
    "    s3 = PropsSI('S','T',T3,'Q',0,fluid) #Specific entropy state 3\n",
    "    p2 = p3 #Pressure state 2\n",
    "    s2 = s1 #Specific entropy state 2\n",
    "    T2 = PropsSI('T','P',p2,'S',s2,fluid) #Temperature state 2\n",
    "    h2 = PropsSI('H','P',p2,'S',s2,fluid) #Specific enthalpy state 2\n",
    "    h4 = h3 #Specific enthalpy state 4\n",
    "    T4 = T1 #Temperature state 4\n",
    "    p4 = p1 #Pressure state 4\n",
    "    s4 = PropsSI('S','P',p4,'H',h4,fluid) #Specific entropy state 4\n",
    "    \n",
    "    cop =(h1-h4)/(h2-h1) #Coefficient of performance\n",
    "    return(cop)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "b64b29a0",
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_cop_simple_ideal(fluid,TL,TH):\n",
    "    cop_hp = cop_hp_simple_ideal(fluid,TL,TH)\n",
    "    cop_r = cop_r_simple_ideal(fluid,TL,TH)\n",
    "    print(f\"Ytelsesfaktor oppvarming: {cop_hp:.3}\")\n",
    "    print(f\"Ytelsesfaktor kjøling: {cop_r:.3}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "ffd6b851",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "4eadc90716654efc81b376bcc5593da2",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(Dropdown(description='Fluid:', options={'R134a': 'R134a', 'Ammoniakk': 'Ammonia', 'Propa…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "widgets.interact(print_cop_simple_ideal, \n",
    "    fluid = widgets.Dropdown(options = fluids, description = \"Fluid:\"),\n",
    "    TL = widgets.FloatSlider(value = 0, min = -30, max = 10, description = \"$T_{\\mathrm{lav}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False),\n",
    "    TH = widgets.FloatSlider(value = 300, min = 20, max = 50, description = \"$T_{\\mathrm{høy}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False)\n",
    "        );"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4cd17319",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>Oppgave E.1</b>\n",
    "<br>Diskuter betydningen av temperaturen i fordamperen, temperaturen i kondensatoren og valg av arbeidsmedium for ytelsesfaktoren.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8da263c",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "**DISKUTER HER!**\n",
    "\n",
    "...\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c32e062a",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## Enkel, reell varmepumpeprosess\n",
    "\n",
    "Dersom man tar hensyn til irreversibiliteter i kompressoren, men for øvrig antar ideell oppførsel (det finnes også andre tap i den reelle varmepumpeprosessen), vil de fire delprossene i den enkle varmepumpeprosessen være som følger:\n",
    "- 1&rarr;2: Kompressor: Adiabatisk kompresjon\n",
    "- 2&rarr;3: Kondensator: Isobar nedkjøling\n",
    "- 3&rarr;4: Strupeventil: Isentalpisk ekspansjon\n",
    "- 4&rarr;1: Fordamper: Isobar oppvarming"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72cb6aed",
   "metadata": {},
   "source": [
    "### Temperatur-entropi-diagram\n",
    "\n",
    "Nedenfor er prosessforløpet i en enkel, reell varmepumpesyklus plottet i et temperatur-entropi-diagram."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "461ca922",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tsdiagram_heatpump_simple_real(fluid,TL,TH,efficiency):\n",
    "    # Plotting ideal simple heat pump cycle in Ts diagram\n",
    "    # fluid: refrigerant (as string)\n",
    "    # TL: Evaporation temperature (°C)\n",
    "    # TH: Condensation temperature (°C) \n",
    "    # efficiency: Isentropic efficiency compressor (%)\n",
    "    # Assuming saturated vapor at evaporator outlet\n",
    "    # Assuming saturated liquid at condenser outlet\n",
    "    \n",
    "    T1 = TL+273.15\n",
    "    h1 = PropsSI('H','T',T1,'Q',1,fluid) #Specific enthalpy state 1\n",
    "    s1 = PropsSI('S','T',T1,'Q',1,fluid) #Specific entropy state 1\n",
    "    p1 = PropsSI('P','T',T1,'Q',1,fluid) #Pressure state 1\n",
    "    T3 = TH+273.15\n",
    "    p3 = PropsSI('P','T',T3,'Q',0,fluid) #Pressure state 3\n",
    "    h3 = PropsSI('H','T',T3,'Q',0,fluid) #Specific enthalpy state 3\n",
    "    s3 = PropsSI('S','T',T3,'Q',0,fluid) #Specific entropy state 3\n",
    "    p2 = p3 #Pressure state 2\n",
    "    s2s = s1 #Specific entropy state 2s\n",
    "    h2s = PropsSI('H','P',p2,'S',s2s,fluid) #Specific enthalpy state 2s\n",
    "    h2 = h1 + (h2s-h1)/(efficiency/100) #Specific enthalpy state 2\n",
    "    s2 = PropsSI('S','P',p2,'H',h2,fluid) #Specific entropy state 2\n",
    "    T2 = PropsSI('T','P',p2,'H',h2,fluid) #Temperature state 2\n",
    "    h4 = h3 #Specific enthalpy state 4\n",
    "    T4 = T1 #Temperature state 4\n",
    "    p4 = p1 #Pressure state 4\n",
    "    s4 = PropsSI('S','P',p4,'H',h4,fluid) #Specific entropy state 4\n",
    "    \n",
    "    compressor_entropy = [s1, s2] #Specific entropy 1->2\n",
    "    compressor_temperature = [T1, T2] #Temperature 1->2\n",
    "    condenser_entropy = np.linspace(s2, s3, 1000) #Entropy 2->3\n",
    "    condenser_temperature = PropsSI('T','P',p2,'S',condenser_entropy,fluid) #Temperature 2->3\n",
    "    valve_entropy = np.linspace(s3, s4, 1000) #Specific entropy 3->4\n",
    "    valve_temperature = PropsSI('T','H',h3,'S',valve_entropy,fluid) #Temperature 3->4\n",
    "    evaporator_entropy = [s1, s4] #Specific entropy 4->1\n",
    "    evaporator_temperature = [T1, T4] #Temperature 4->1\n",
    " \n",
    "    tsdiagram(fluid)\n",
    "    plt.plot(compressor_entropy,compressor_temperature,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(condenser_entropy,condenser_temperature,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(valve_entropy,valve_temperature,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    plt.plot(evaporator_entropy,evaporator_temperature,color='C3',linewidth='2', marker = \"o\", markersize = 10, mec = \"C3\", mfc = \"C3\", markevery = [0,-1])\n",
    "    \n",
    "    plt.annotate(\"1\", (s1,T1), (s1+40,T1), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"2\", (s2,T2), (s2,T2+40), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"3\", (s3,T3), (s3-80,T3), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})\n",
    "    plt.annotate(\"4\", (s4,T4), (s4-80,T4), ha=\"center\", va=\"center\", color = 'C2', size = '20', arrowprops = {\"arrowstyle\" : \"-|>\", \"color\": \"C2\"})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "264b62d7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "c0c4743f84f54118abc1dcb2ae724733",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(Dropdown(description='Fluid:', options={'R134a': 'R134a', 'Ammoniakk': 'Ammonia', 'Propa…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "#Interactive plot of real simple heat pump cycle in Ts diagram\n",
    "widgets.interact(tsdiagram_heatpump_simple_real, \n",
    "    fluid = widgets.Dropdown(options = fluids, description = \"Fluid:\"),\n",
    "    TL = widgets.FloatSlider(value = 0, min = -30, max = 10, description = \"$T_{\\mathrm{lav}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False),\n",
    "    TH = widgets.FloatSlider(value = 30, min = 20, max = 50, description = \"$T_{\\mathrm{høy}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False),\n",
    "    efficiency = widgets.FloatSlider(value = 80, min = 60, max = 100, description = \"$\\eta_{\\mathrm{s}} (\\%)$\", disabled = False, continuous_update = False)\n",
    "                );"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "dbb44e5f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def cop_hp_simple_real(fluid,TL,TH,efficiency):\n",
    "    # Calculating coefficient of performance of ideal simple heat pump cycle\n",
    "    # fluid: refrigerant (as string)\n",
    "    # TL: Evaporation temperature (°C)\n",
    "    # TH: Condensation temperature (°C)\n",
    "    # efficiency: Isentropic efficiency compressor (%)\n",
    "    # Assuming saturated vapor at evaporator outlet\n",
    "    # Assuming saturated liquid at condenser outlet\n",
    "    \n",
    "    T1 = TL+273.15\n",
    "    s1 = PropsSI('S','T',T1,'Q',1,fluid) #Specific entropy state 1\n",
    "    p1 = PropsSI('P','T',T1,'Q',1,fluid) #Pressure state 1\n",
    "    h1 = PropsSI('H','T',T1,'Q',1,fluid) #Specific enthalpy state 1\n",
    "    T3 = TH+273.15\n",
    "    p3 = PropsSI('P','T',T3,'Q',0,fluid) #Pressure state 3\n",
    "    h3 = PropsSI('H','T',T3,'Q',0,fluid) #Specific enthalpy state 3\n",
    "    s3 = PropsSI('S','T',T3,'Q',0,fluid) #Specific entropy state 3\n",
    "    p2 = p3 #Pressure state 2\n",
    "    s2s = s1 #Specific entropy state 2s\n",
    "    h2s = PropsSI('H','P',p2,'S',s2s,fluid) #Specific enthalpy state 2s\n",
    "    h2 = h1 + (h2s-h1)/(efficiency/100) #Specific enthalpy state 2\n",
    "    h4 = h3 #Specific enthalpy state 4\n",
    "    T4 = T1 #Temperature state 4\n",
    "    p4 = p1 #Pressure state 4\n",
    "    s4 = PropsSI('S','P',p4,'H',h4,fluid) #Specific entropy state 4\n",
    "    \n",
    "    cop =(h2-h3)/(h2-h1) #Coefficient of performance\n",
    "    return(cop)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "f1bb7a77",
   "metadata": {},
   "outputs": [],
   "source": [
    "def cop_r_simple_real(fluid,TL,TH,efficiency):\n",
    "    # Calculating coefficient of performance of ideal simple heat pump cycle\n",
    "    # fluid: refrigerant (as string)\n",
    "    # TL: Evaporation temperature (°C)\n",
    "    # TH: Condensation temperature (°C)\n",
    "    # efficiency: Isentropic efficiency compressor (%)\n",
    "    # Assuming saturated vapor at evaporator outlet\n",
    "    # Assuming saturated liquid at condenser outlet\n",
    "    \n",
    "    T1 = TL+273.15\n",
    "    s1 = PropsSI('S','T',T1,'Q',1,fluid) #Specific entropy state 1\n",
    "    p1 = PropsSI('P','T',T1,'Q',1,fluid) #Pressure state 1\n",
    "    h1 = PropsSI('H','T',T1,'Q',1,fluid) #Specific enthalpy state 1\n",
    "    T3 = TH+273.15\n",
    "    p3 = PropsSI('P','T',T3,'Q',0,fluid) #Pressure state 3\n",
    "    h3 = PropsSI('H','T',T3,'Q',0,fluid) #Specific enthalpy state 3\n",
    "    s3 = PropsSI('S','T',T3,'Q',0,fluid) #Specific entropy state 3\n",
    "    p2 = p3 #Pressure state 2\n",
    "    s2s = s1 #Specific entropy state 2s\n",
    "    h2s = PropsSI('H','P',p2,'S',s2s,fluid) #Specific enthalpy state 2s\n",
    "    h2 = h1 + (h2s-h1)/(efficiency/100) #Specific enthalpy state 2\n",
    "    h4 = h3 #Specific enthalpy state 4\n",
    "    T4 = T1 #Temperature state 4\n",
    "    p4 = p1 #Pressure state 4\n",
    "    s4 = PropsSI('S','P',p4,'H',h4,fluid) #Specific entropy state 4\n",
    "    \n",
    "    cop =(h1-h4)/(h2-h1) #Coefficient of performance\n",
    "    return(cop)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "26331899",
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_cop_simple_real(fluid,TL,TH,efficiency):\n",
    "    cop_hp = cop_hp_simple_real(fluid,TL,TH,efficiency)\n",
    "    cop_r = cop_r_simple_real(fluid,TL,TH,efficiency)\n",
    "    print(f\"Ytelsesfaktor oppvarming: {cop_hp:.3}\")\n",
    "    print(f\"Ytelsesfaktor kjøling: {cop_r:.3}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "360128f2",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "648d8105d7de4917aedbf74d8f1f9921",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(Dropdown(description='Fluid:', options={'R134a': 'R134a', 'Ammoniakk': 'Ammonia', 'Propa…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "widgets.interact(print_cop_simple_real, \n",
    "    fluid = widgets.Dropdown(options = fluids, description = \"Fluid:\"),\n",
    "    TL = widgets.FloatSlider(value = 0, min = -30, max = 10, description = \"$T_{\\mathrm{lav}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False),\n",
    "    TH = widgets.FloatSlider(value = 300, min = 20, max = 50, description = \"$T_{\\mathrm{høy}} (\\mathrm{\\u00B0C})$\", disabled = False, continuous_update = False),\n",
    "    efficiency = widgets.FloatSlider(value = 80, min = 60, max = 100, description = \"$\\eta_{\\mathrm{s}} (\\%)$\", disabled = False, continuous_update = False)\n",
    "                );"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "be92b426",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>Oppgave E.2</b>\n",
    "<br>Finn ytelsesfaktoren for den enkle varmepumpeprosessen analysert i Oppgave 2 og 3 i Øving 11, sammenlign resulatene med resulatene fra Øving 11 og diskuter betydningen av den isentropiske virkningsgraden for ytelsesfaktoren.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a69e8745",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "**DISKUTER HER!**\n",
    "\n",
    "...\n",
    "\n",
    "---"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
